#!/usr/bin/env python
# coding: utf-8

# In[39]:


import pandas as pd
import matplotlib.pyplot as plt
from statistics import mean
import numpy as np


# In[4]:


health = pd.read_csv('LGA_by_HEALTH.csv')


# In[63]:


#average the health issues across Victoria
average_health_issues = {'smokers': health['smokers'].mean(),
                         'heart_disease': health['heart_disease'].mean(),
                         'pre_obese': health['pre_obese'].mean(),
                         'obese': health['obese'].mean()
                        }
average_health_victoria= pd.Series(average_health_issues)
average_health_victoria.index.name = 'Health_factor'


# In[33]:


#pm10 average added to health
pm10 = pd.read_csv('PM10_AV_READING.csv')
health['pm10'] = pm10['value']
health.set_index('LGA_CODE_2011')


# In[52]:


#correlation between pm10 and smokers
correlation_smokers = np.corrcoef(health['smokers'], health['pm10'])
#correlation between pm10 and obesity
correlation_obese = np.corrcoef(health['obese'], health['pm10'])
#correlation between pm10 and heart_disease
correlation_heart_disease = np.corrcoef(health['heart_disease'], health['pm10'])
#correlation between pm10 and pre obese
correlation_pre_obese = np.corrcoef(health['pre_obese'], health['pm10'])


# In[67]:


cor_obese=correlation_obese[1][0]
cor_heart_disease=correlation_heart_disease[1][0]
cor_pre_obese=correlation_pre_obese[1][0]
cor_smokers=correlation_smokers[1][0]


# In[62]:


correlations_pm10 = {'smokers': cor_smokers,
                     'heart_disease': cor_heart_disease,
                     'pre_obese': cor_pre_obese,
                     'obese': cor_obese
                    }
correlations_pm10 = pd.Series(correlations_pm10)
correlations_pm10.index.name='Health_factor'


# In[70]:


Victoria = pd.DataFrame({'Average': average_health_victoria, 
                        'Correlation_pm10':correlations_pm10})
Victoria


# In[72]:


Victoria.to_csv('health_average_correlation_pm10', index = True)

